
var element = $('.list');
var addButton = $('.add');
var deleteButton = $('.delete');
var input = $('input');
var itemList;
var items = [];

class Item {
    constructor(title, element) {
        this.title = title;
        this.element = element;
        this.isClicked = false;
        this.element.on('click', this.onClick.bind(this));
    }
    render() {
        let element = $(this.element);
        if (this.isClicked == true) {
            this.element.css('background-color', 'grey');
        } else {
            this.element.css('background-color', 'white');
        }
        element.html(this.title);
    }

    onClick() {
        this.isClicked = !this.isClicked;
        this.render();
    }
}

class ItemList {
    constructor(element) {
        this.items = [];
        this.element = element;
    }
    render() {
        if (this.items.lenght > 0) {
            this.items.map((item) => item.render());
        }
    }
    add(title) {
        var div = $('<div></div>');
        var newItem = new Item(title, div);
        this.items.push(newItem);
        this.element.append(newItem.element);
        newItem.render();
        input.focus();
    }

    remove() {
        for (let i = this.items.length - 1; i >= 0; i--) {
            if (this.items[i].isClicked) {
                this.items[i].element.remove();
                this.items.splice(i, 1);
                input.focus();
            }
        }
    }
}

itemList = new ItemList(element);

addButton.on('click', function() {
    itemList.add(input.val());
    input.val('');
});

deleteButton.on('click', function() {
    itemList.remove();
});